# HeyDJ

HeyDJ is a slack bot to play, queue music.

## Getting Started

### 1. Install dependencies.
```
pip install slackbot vlc pafy flask youtube-dl
```


### 2. Install VLC Player.
  [Download VLC Player](https://www.videolan.org/vlc/).

  Architecture of VLC Player and Python **should be same**.

  If your **Python** is **x64** then **VLC Player** must be **x64** too.


### 3. Create Slack Bot Token

  1. Create a [Slack app](https://api.slack.com/apps?new_app=1)

  1. Add a Bot User and configure your bot user with some basic info (display name, default username and its online presence)

  1. Once you've completed these fields, click **Add Bot User**

  1. Next, give your bot access to the [Events API](https://api.slack.com/bot-users#setup-events-api)

  1. Finally, add your [bot to your workspace](https://api.slack.com/bot-users#installing-bot)

> More details https://slack.com/intl/en-vn/help/articles/115005265703


### 4. Put your slackbot token to configuration.

Rename the `slackbot_settings.py_default` to `slackbot_settings.py` fill up the API token starting with `xoxb`
```
API_TOKEN = "xoxb-something-very-long-string"
```


### 5. Create database
  1. Change code to create `playlist.db`

    Open webserver.py, and goto bottom line where `myPlayer.runOnce(False)`
Change `False` to `True`

  1. On the command line:

    ```C:\heydj> python webserver.py```

  1. Once the `playlist.db` is created, change the code back to `False`

### 6. Finally, Run!!

```
C:\heydj> python start.py
```


- - - -
## About Internal Structure
HeyDJ relies on vlc library and flask for playing music and communicating with bot service. 

webserver.py receives `GET` request and runs the command.
botserver.py receives slack message from user using API, and sends http request to webserver and receives the return, gives user feed back in message format.


### webserver.py
This is a mixture of vlc, sqlite3 and flask.
When request is received from the http, it will respond in json.

#### vlc
I'm using vlc on MediaPlayer and MediaListPlayer.
MediaListPlayer is a wrapper of MediaPlayer instance, with a playlist in in.
I tried to use ony MediaPlayer, yet was unable to accomplish, so I use the MediaListPlayer to dynamically add music in end of the playlist, when I get the event from `vlc.EventType.MediaPlayerEndReached`

#### User request limit
Basically Slack doesn't allow to send message more than 1 per second. Although, to avoid the abuse of adding the music or skipping other's music, I hard-coded `maxReqCount`, and `maxReqTime`. 

### playlist.db
This database stores userdata and playlist requested from user.
Index of current playing media is also saved to `variables.currentIdx`.

### botserver.py
This opens up slack bot session using Real Time Messaging API, due to the firewall restriction.
Also receives the message and parse them and send request to `webserver.py`.

### start.py
You can start webserver.py and botserver.py separately

In command line window:
```
C:\heydj> python webserver.py
```
Open another command line window:
```
C:\heydj> python botserver.py
```

However, you can run them both at once like:
```
C:\heydj> python start.py
```





        self.default_MSG=[
            {
                "type": "section",
                "text": {
                    "type": "mrkdwn",
                    "text": "Hi, This is _*HeyDJ*_. Your music assistant for SNA Development Division. Please feel free to add any music to the queue. \n*Here are next 5 songs will be played in my playlist.*"
                }
            },
            {
                "type": "divider"
            },
            {
                "type": "section",
                "text": {
                    "type": "mrkdwn",
                    "text": "*J Rabbit - 웃으며 넘길래*\n:star::star::star::star: 4.9 00:03:56\nAdded by ikko"
                },
                "accessory": {
                    "type": "image",
                    "image_url": "http://i.ytimg.com/vi/pWF1dNBkldU/mqdefault.jpg",
                    "alt_text": "alt text for image"
                }
            },
            {
                "type": "divider"
            },
            {
                "type": "actions",
                "elements": [
                    {
                        "type": "button",
                        "text": {
                            "type": "plain_text",
                            "text": ":heavy_plus_sign: Add",
                            "emoji": True
                        },
                        "value": "add"
                    },
                    {
                        "type": "button",
                        "text": {
                            "type": "plain_text",
                            "text": ":arrow_forward: Play",
                            "emoji": True
                        },
                        "value": "play"
                    },
                    {
                        "type": "button",
                        "text": {
                            "type": "plain_text",
                            "text": ":double_vertical_bar: Pause",
                            "emoji": True
                        },
                        "value": "pause"
                    },
                    {
                        "type": "button",
                        "text": {
                            "type": "plain_text",
                            "text": ":fast_forward: Skip",
                            "emoji": True
                        },
                        "value": "next"
                    }
                ]
            },
            {
                "type": "context",
                "elements": [
                    {
                        "type": "mrkdwn",
                        "text": "Total *2131* items in playlist, total of *123813* seconds."
                    }
                ]
            },
            {
                "type": "context",
                "elements": [
                    {
                        "type": "mrkdwn",
                        "text": "Last updated: Jan 1, 2019"
                    }
                ]
            }
        ]
        

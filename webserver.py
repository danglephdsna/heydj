#! /usr/bin/python
# -*- coding: utf-8 -*-

# 1. install dependency.
# pip install python-vlc pafy youtube-dl flask

# 2. go to https://www.videolan.org/vlc/ download and install VLC Player.
# requires to set environment variable for VLC Library
import sys, os
os.environ['PYTHON_VLC_MODULE_PATH'] = """C:\Program Files\VideoLan"""
os.environ['PYTHON_VLC_LIB_PATH'] = """C:\Program Files\VideoLan\VLC\libvlc.dll"""

# vlc CType Bindings
import vlc

# Youtube 
import pafy

# Flask Web Service
from flask import Flask, make_response, request, redirect

# saving and managing playlist
import sqlite3, json

# datetime calculation
from datetime import datetime, timedelta



class myPlayerClass():

    def __init__(self):
        self.dbfile="playlist.db"
        self.conn=self.connectDB()

        self.vlcInstance=vlc.Instance()
        # self.vlcInstance=vlc.Instance("--run-time=10")
        self.mlp=self.vlcInstance.media_list_player_new()
        self.mp=self.vlcInstance.media_player_new()
        self.mlp.set_media_player(self.mp)

        self.mp_em = self.mp.event_manager()
        self.mp_em.event_attach(vlc.EventType.MediaPlayerEndReached, self.cb)
        self.mp_em.event_attach(vlc.EventType.MediaListPlayerNextItemSet , self.cb)

        self.ml=vlc.MediaList()
        self.ml_em = self.ml.event_manager()
        self.ml_em.event_attach(vlc.EventType.MediaListItemAdded, self.cb)

        # User limit
        self.maxReqCount=10
        self.maxReqTime=timedelta(minutes=60)
        
        try:
            self.currentIdx=self.getCurrentIdx()
            self.videoId=self.getVideoIdByIdx(self.currentIdx)
            self.mediaInfo=self.getMediaInfo(self.videoId)
        except:
            self.currentIdx=None
            self.videoId=None
            self.mediaInfo=None

    @vlc.callbackmethod
    def cb(self, event):
        print("callback:",event.type)

        if event.type in [vlc.EventType.MediaPlayerMediaChanged]:
            pass

        elif event.type in [vlc.EventType.MediaListItemAdded]:
            self.mlp.set_media_list(self.ml)
            self.mlp.play()

        elif event.type in [vlc.EventType.MediaPlayerEndReached]:
            self.currentIdx+=1
            self.setCurrentIdx(self.currentIdx)
            self.videoId=self.getVideoIdByIdx(self.currentIdx)
            self.mediaInfo=self.getMediaInfo(self.videoId)
            self.ml.add_media(self.mediaInfo['mrl'])
        return

    def connectDB(self):
        conn=None
        try:
            conn = sqlite3.connect(self.dbfile, detect_types=sqlite3.PARSE_DECLTYPES | 
                sqlite3.PARSE_COLNAMES, check_same_thread=False)
            conn.row_factory = sqlite3.Row

        except Error as e:
            print(e)
        return conn

    def executeSQL(self, sql):
        try:
            cursor = self.conn.cursor()
            cursor.execute(sql)
            self.conn.commit()
        except Error as e:
            print(e)

    def runOnce(self, param=False):
        if param is True:
            print (self.conn)
            if self.conn is not None:
                print("!!!WARNING!!! Closing Database...")
                self.conn.close()

            print("os.path.isfile(self.dbfile)", os.path.isfile(self.dbfile))
            if os.path.isfile(self.dbfile):
                print("!!!WARNING!!! Removing Database...")
                os.remove(self.dbfile)

            print("!!!WARNING!!! Re-Initilizing Database...")
            self.conn = self.connectDB()

            with open('initdb.sql','r', encoding='utf-8') as fp:
                cursor=self.conn.cursor()
                cursor.executescript(fp.read())
                self.conn.commit()
                self.conn.close()
            print("Initialization Complete.")
            print("Please set runOnce(False) in __main__ to correctly run server.")
            shutdown = request.environ.get('werkzeug.server.shutdown')
            if func is None:
                raise RuntimeError('Not running with the Werkzeug Server')
            shutdown()

            sys.exit()

    def getMediaInfo(self, videoId):
        retval=None
        if videoId is not None:
            url = "https://www.youtube.com/watch?v="+videoId
            media = pafy.new(url)
            mrl = media.getbest().url

            retval={
                "url": url,
                "mrl": mrl,
                "videoId": media.videoid,
                "title": media.title,
                "thumbs": media.bigthumb,
                "duration": media.duration,
                "rating": media.rating,
            }
        self.mediaInfo=retval
        return retval

    def getVideoIdByIdx(self, playlistIdx):
        # print ("getting IDX :%s" %playlistIdx)
        cursor=self.conn.cursor()
        cursor.execute("""
            SELECT videoId FROM playlist WHERE id=%s;
            """% playlistIdx)
        try:
            videoId=cursor.fetchone()['videoId']
        except:
            videoId=None
        return videoId

    def getCurrentIdx(self):
        cursor=self.conn.cursor()
        cursor.execute("""
            SELECT count(playlist.id) AS count, variables.value AS idx
            FROM playlist, variables
            WHERE variables.key="currentIdx";
        """)
        row=cursor.fetchone()
        if row['idx'] is None:
            idx=1
        else:
            idx=int(row['idx'])
            maxIdx=row['count']
            if idx > maxIdx:
                idx=1
        self.currentIdx=idx

        # print ("**************************** currentIdx:",idx)
        return idx

    def setCurrentIdx(self, idx):
        cursor=self.conn.cursor()
        cursor.execute("""
            SELECT count(playlist.id) AS count, variables.value AS idx
            FROM playlist, variables
            WHERE variables.key="currentIdx";
        """)
        maxIdx=cursor.fetchone()['count']
        idx=int(idx) #variables are always string
        if idx > maxIdx:
            idx=1
        self.currentIdx=idx

        cursor.execute("""
            UPDATE variables SET value = '%s' WHERE key='currentIdx';
        """ % str(idx))
        self.conn.commit()
        return True

    def add(self, videoId, userId, userName):
        retval = self.getMediaInfo(videoId)
        retval['addDate']= datetime.now()
        retval['result']="added"
        retval['addUser']=userId
        retval['userName']=userName
        retval['server']='fail'

        # check `users` table with userId.
        proceed, retval['msg'] = self.checkUser(userId, userName)
        # if update date and count is > 10 then cancel the operation.

        if proceed is True:
            cursor=self.conn.cursor()
            cursor.execute("""
                INSERT INTO 'playlist' (
                    videoId, addDate, title, thumbs, duration, rating, addUser
                )
                VALUES (
                    :videoId, :addDate, :title, :thumbs, :duration, :rating, :addUser
                );
                """, retval)
            self.conn.commit()
            retval['server']='success'

        # cursor.execute("""
        #     /* if user doesn't exist in users table add user to `users`
        #     if user exist update count and date.
        #     */
        #     """)

        return retval

    def status(self):
        retval=self.mediaInfo
        if retval==None:
            self.currentIdx=self.getCurrentIdx()
            videoId=self.getVideoIdByIdx(self.currentIdx)
            retval=self.getMediaInfo(videoId)
        retval['currentIdx']=self.currentIdx
        retval['volume']=self.mp.audio_get_volume()

        mpState=None
        if self.mp is not None:
            mpState=self.mp.get_state()
        if mpState in [vlc.State.Playing, vlc.State.Paused]:
            current_mm, current_ss = divmod(self.mp.get_length()*self.mp.get_position()/1000,60)
            current_hh, current_mm = divmod(current_mm,60)
            retval['playing_at']="%02d:%02d:%02d"%(int(current_hh), int(current_mm), int(current_ss))
            retval['status']=str(mpState).replace("State.","").lower()
        
        retval['status']=str(mpState).replace("State.","").lower()
        retval['server']='success'
        return retval

    def queue(self):
        retval=None
        cursor=self.conn.cursor()
        # result=cursor.execute('SELECT id, videoId, title, thumbs, duration, rating, addUser FROM playlist ORDER BY id ASC;')
        result=cursor.execute("""
            SELECT id, videoId, title, thumbs, time(duration) AS duration, rating, addUser 
            FROM playlist 
            WHERE id >= {} 
            ORDER BY id ASC;""".format(self.currentIdx))
        items=[]

        remainingSec=0
        for row in result:
            items.append({
                'id':row['id'],
                'videoId':row['videoId'],
                'title':row['title'],
                'thumbs':row['thumbs'],
                'duration':row['duration'],
                'rating':row['rating'],
                'addUser':row['adduser']
            })
            t=datetime.strptime(row['duration'],"%H:%M:%S")
            remainingSec+=(t.second*1)+(t.minute*60)+(t.hour*60*60)
        # items=[dict(zip([key[0] for key in cursor.description], row)) for row in result]
        retval={"items":items, "remainingSec":remainingSec}
        retval['server']='success'
        return retval

    def skip(self, event=None, status=1, userId=None, userName=None):
        # print ("Playing Next media")
        retval=self.status()
        if userId is None:
            proceed = True
        else:
            proceed, retval['msg'] = self.checkUser(userId, userName)

        if proceed is False:
            retval['server']='fail'
        else:
            if self.currentIdx == None:
                self.currentIdx = self.getCurrentIdx()

            self.setCurrentIdx(self.currentIdx+1)
            self.currentIdx=self.getCurrentIdx()
            
            self.videoId=self.getVideoIdByIdx(self.currentIdx)
            self.mediaInfo=self.getMediaInfo(self.videoId)

            self.ml.add_media(self.mediaInfo['mrl'])
            self.mlp.set_media_list(self.ml)

            self.mlp.next()
            retval=self.status()
            # print (self.mediaInfo)
        print(retval)
        return retval

    def pause(self):
        self.mlp.pause()
        return self.status()

    def stop(self):
        self.mlp.stop()
        return self.status()

    def play(self):
        flag_play=True
        # print(self.mlp.get_state())

        try:
            if self.mlp.get_state() in [vlc.State.Playing]:
                # print("Player is still playing")
                flag_play=False
        except:
            pass

        if flag_play:

            if self.currentIdx==None or self.mediaInfo==None:

                self.currentIdx=self.getCurrentIdx()
                self.videoId=self.getVideoIdByIdx(self.currentIdx)
                self.mediaInfo=self.getMediaInfo(self.videoId)

            self.ml.add_media(self.mediaInfo['mrl'])
            self.mlp.set_media_list(self.ml)
            self.mlp.play()

        retval=self.status()
        return retval

    def volume(self, volume=None):
        if volume is not None:
            self.mp.audio_set_volume(int(volume))
        return myPlayer.status()

    def checkUser(self, userId, userName=None):
        cursor=self.conn.cursor()
        result=None

        userInfo={
            'id': userId,
            'lastActionTime': datetime.now(),
            'actionCount': 0,
            'displayName': userName,
        }

        ## try to get the current user's info.
        sql="""
            SELECT id, lastActionTime, actionCount, displayName 
            FROM users
            WHERE id = '%s' ORDER BY lastActionTime DESC LIMIT 0,1;""" % userInfo['id']
    
        cursor.execute(sql)
        row=cursor.fetchone()

        ## if user does not exists, then create current user's info.
        if row is None:
            cursor.execute("""
                INSERT INTO 'users' (
                    id, lastActionTime, actionCount, displayName
                )
                VALUES (
                    :id, :lastActionTime, :actionCount, :displayName
                );
            """, userInfo)
            self.conn.commit()
        else:
            userInfo['lastActionTime']=row['lastActionTime']
            userInfo['actionCount']=row['actionCount']
            userInfo['displayName']=row['displayName']
       

        timelimit=userInfo['lastActionTime'] + self.maxReqTime
        # now update user if user action count and date is resonable.
        if userInfo['actionCount'] >= self.maxReqCount and datetime.now() < timelimit:
            msg = "Sorry {}, you cannot perform this action due to too many request\n".format(userInfo['displayName'])
            msg +="Please try again after {}".format(timelimit)
            return False, msg
        else:
            if userInfo['actionCount'] >= self.maxReqCount:
                userInfo['actionCount']=0

            userInfo['actionCount']+=1
            userInfo['lastActionTime']=datetime.now()

            cursor.execute("""
                UPDATE users SET lastActionTime=?, actionCount=?  WHERE id=?;""",
                (userInfo['lastActionTime'], userInfo['actionCount'], userInfo['id'])
            )
            self.conn.commit()

            msg = "Your have %d request available." % (self.maxReqCount - userInfo['actionCount'])
            return True, msg


app = Flask (__name__)

@app.route("/")
def home():
    return json.dumps({"title":"Welcome to Hello DJ"})

@app.route("/play")
def http_play():
    return myPlayer.play()

@app.route("/stop")
def http_stop():
    myPlayer.stop()
    return json.dumps(myPlayer.status())

@app.route("/pause")
def http_pause():
    # myPlayer.pause()
    # return make_response("OK",200)
    return myPlayer.pause()

@app.route("/next", methods=['GET'])
def http_skip():
    # myPlayer.skip()
    userId=request.args.get('u')
    userName=request.args.get('n')
    return myPlayer.skip(userId=userId, userName=userName)

@app.route("/status")
def http_status():
    return myPlayer.status()

@app.route("/queue")
def http_queue():
    playlist = myPlayer.queue()
    retval = json.dumps(playlist)
    return retval

@app.route("/volume")
def http_setVolume():
    methods=['GET']
    vol=request.args.get('l')
    return myPlayer.volume(vol)
    # return myPlayer.status()

@app.route("/add", methods=['GET'])
def http_add():
    videoId=request.args.get('v')
    userId=request.args.get('u')
    userName=request.args.get('n')
    return myPlayer.add(videoId, userId, userName)

@app.route("/ping")
def http_ping():
    return {'server':'success', 'msg':'Webserver is alive'}


if __name__ == "__main__":
    myPlayer = myPlayerClass()
    myPlayer.runOnce(False)
    app.run(debug=True, threaded=True)

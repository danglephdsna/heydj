#! /usr/bin/python
# -*- coding: utf-8 -*-

# slack integration
# https://github.com/lins05/slackbot
# pip install slackbot
from slackbot.bot import Bot, respond_to, listen_to, default_reply

# datetime calculation
from datetime import datetime, timedelta
import re, json
import requests

import time

from urllib.parse import urlparse, parse_qs


server="http://127.0.0.1:5000"

def mpMsgHandler(mediaInfo):
    # mediaInfo=json.loads(jsonStr)
    # print (mediaInfo)
    replyMsg="""
        media['title']
        media['thumbs']
        media['duration']
        media['rating']
        media['addUser']
        media['addedDate']
    """
    return mediaInfo

@listen_to('hi')
def hi(message):
    message.reply('I can understand hi or HI')
    message.react('+1')

@respond_to('I love you')
def love(message):
    message.reply('I love you too!')

@listen_to('Can someone help me?')
def help(message):
    # Message is replied to the sender (prefixed with @user)
    message.reply('Yes, I can!')

    # Message is sent on the channel
    message.send('I can help everybody!')

    # Start a thread on the original message
    message.reply("Here's a threaded reply", in_thread=True)

@respond_to("http(.*)", re.IGNORECASE)
def bot_add(message, url=None):
    youtubeId=None
    ### Because of the bug of slackbot.bot library. it returns '<'+str+'>'.
    ### So I'm removing it.
    ### Also need add 'http' in front of url
    ### because library removes http(.*) from message.
    url="http"+url[1:-1]

    if url.startswith("http") >= 0:
        res=urlparse(url)
        if res.netloc == "www.youtube.com":
            # Url starts with www.youtube.com
            youtubeId=parse_qs(res.query)['v'][0]

        if res.netloc == "youtu.be":
            # Url starts with youtu.be
            youtubeId=res.path[1:]

    if youtubeId is None:
        message.reply("Sorry I only can understand youtube link")
    else:
        userid=message._get_user_id()
        username=message._client.users[userid]['name']

        response=requests.get(server+"/add", {'v':youtubeId, 'u':userid, 'n':username})
        mediaInfo=mpMsgHandler(response.json())
        if mediaInfo['server'] == 'success':
            msg= "Added: *{}* - {}\n".format(mediaInfo['title'], mediaInfo['duration'])
            response=requests.get(server+"/queue")
            mediaInfo=mpMsgHandler(response.json())
            msg+="Your song will be played after {} seconds".format(mediaInfo['remainingSec'])
        else:
            msg=mediaInfo['msg']
        message.reply(msg)

@listen_to("http(.*)", re.IGNORECASE)
def bot_public_add(message, url=None):
    message.reply("Please send private message: @HeyDJ <http://youtubeurl>")

@listen_to('play')
def bot_play(message):
    response=requests.get(server+"/play")
    mediaInfo=mpMsgHandler(response.json())
    msg= "Playing: *{}* - {}\n".format(mediaInfo['title'], mediaInfo['duration'])
    msg+=" Queue: {}".format(mediaInfo['currentIdx'])
    message.send(msg)

@listen_to("stop", re.IGNORECASE)
def bot_stop(message):
    response=requests.get(server+"/stop")
    mediaInfo=mpMsgHandler(response.json())

    msg= "Stoppd: *{}* - {}\n".format(mediaInfo['title'], mediaInfo['duration'])
    msg+=" Queue: {}".format(mediaInfo['currentIdx'])
    message.send(msg)

@listen_to("pause")
def bot_pause(message):
    response=requests.get(server+"/pause")
    mediaInfo=mpMsgHandler(response.json())

    msg= "Paused: *{}* - {}\n".format(mediaInfo['title'], mediaInfo['duration'])
    msg+=" Queue: {}".format(mediaInfo['currentIdx'])
    message.send(msg)

@listen_to("next")
@listen_to("skip")
def bot_skip(message):
    userid=message._get_user_id()
    username=message._client.users[userid]['name']

    # print("userid:", userid, "username", username)

    response=requests.get(server+"/next", {'u':userid, 'n':username})

    mediaInfo=mpMsgHandler(response.json())
    if mediaInfo['server'] == 'success':
        msg="Next Up ({}) *{}* - {}\n".format(mediaInfo['currentIdx'], mediaInfo['title'], mediaInfo['duration'])
    else:
        msg=mediaInfo['msg']
    message.send(msg)

@listen_to("status")
def bot_status(message):
    response=requests.get(server+"/status")
    mediaInfo=mpMsgHandler(response.json())
    msg=None
    try:
        playingAt=mediaInfo['playing_at']    
    except:
        playingAt="Stopped"

    msg="{}: *{}* - {}\n".format(mediaInfo['status'], mediaInfo['title'], playingAt)
    msg+=" Queue: {}".format(mediaInfo['currentIdx'])

    message.send(msg)

@listen_to("list")
def bot_queue(message):
    print("UserID: ",message._get_user_id())
    response=requests.get(server+"/queue")
    mediaInfo=mpMsgHandler(response.json())
    msg="This is current playlist up to next 10 items\n"
    for item in mediaInfo['items']:
        msg+="{}: *{}* - {}\n".format(item['id'], item['title'], item['duration'])
    message.send(msg)

@listen_to("vol (.*)")
def bot_volume(message, volume):
    # volume=int(volume[1:-1])
    print("rx_volume", volume)
    response=requests.get(server+"/volume", {'l':volume} )
    response=requests.get(server+"/volume")
    mediaInfo=mpMsgHandler(response.json())
    msg="Volume is now *{}*\n".format(mediaInfo['volume'])
    message.send(msg)

@listen_to("bug")
def bot_bug(message):
    msg= "If you find a bug, please fix it and send me a pull request.\n"
    msg+="*Jira*: https://snaglobal.atlassian.net/secure/RapidBoard.jspa?projectKey=HDJ\n"
    msg+="*BitBucket*: https://bitbucket.org/1kko/heydj/"
    message.send(msg)

@default_reply
@listen_to("help")
def bot_help(message):
    msg="Just give me a youtube link then I'll add to the playlist.\n"
    msg+="You can also *play*, *pause*, *stop*, *skip*, *status*, *list*, *vol {0~100}*, *help*, *bug*"
    message.send(msg)

@listen_to("ping")
def ping(message=None):
    response=requests.get(server+"/ping")
    mediaInfo=mpMsgHandler(response.json())
    if message is None:
        print(mediaInfo['msg'])
    else:
        message.send(mediaInfo['msg'])


if __name__ == "__main__":
    print("starting bot")
    time.sleep(5) # lazy loading. give time webserver.py to start first
    heyDJ=Bot()
    ping()
    heyDJ.run()
    print("I'm up and running")

